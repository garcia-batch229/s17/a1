/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

 function printUserInfo(){

	
	let fullName = prompt("What is your name?"); 
	let age = prompt("How old are you?"); 
	let address = prompt("Where do you live?");
	alert("Thank you for your input!");

	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old."); 
	console.log("You live in " + address); 
	 
}

printUserInfo();

function favoriteBand() {
	let band = ["The Beatles", "Metallica", "The Eagles", "L'arc`en`Ciel", "Eraserheads"];


	console.log("1. " + band[0]);
	console.log("2. " + band[1]);
	console.log("3. " + band[2]);
	console.log("4. " + band[3]);
	console.log("5. " + band[4]);
}
favoriteBand();

function movies() {
	let favoriteMovies = ["The Godfather", "The Godfather, Part II", "Shawshank Redemption", "To Kill A Mockingbird", "Psycho"];
	
	let movieRatings = [97, 96, 93, 91];

	console.log("1." + favoriteMovies[0]);
	console.log("Rotten Tomatoes Ratings: " + movieRatings[0] + "%");
	console.log("2." + favoriteMovies[1]);
	console.log("Rotten Tomatoes Ratings: " + movieRatings[1] + "%");
	console.log("3." + favoriteMovies[2]);
	console.log("Rotten Tomatoes Ratings: " + movieRatings[3] + "%");
	console.log("4." + favoriteMovies[3]);
	console.log("Rotten Tomatoes Ratings: " + movieRatings[2] + "%");
	console.log("5." + favoriteMovies[4]);
	console.log("Rotten Tomatoes Ratings: " + movieRatings[1] + "%");
	
}

movies();



let returnFunction = function printUsers(){

	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

returnFunction();